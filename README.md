# zfs role

Install zfs and create zpools on Ubuntu.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Dependencies


_This role does not depend on other Ansible roles._


## Role Variables

* `variable_name`

    Description of variable

    Default value:

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: zfs
```
